<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;
use Illuminate\Support\Facades\Validator;

class CategoriaController extends Controller
{

    //Accion:UN METODO DEL CONTROLADOR, contiene el codigo a ejecutar
    //Nombre:Pueder ser cualquiera
    public function index(){
        //seleccionar categorias existentes
        $categorias=Categoria::paginate(5);
        //enviar la coleccion
        //y las vamos a mostrar
        return view('categorias.index')->with("categorias" ,$categorias);
    }

    //Mostrar el formulario de añadir categoria
    public function create(){

        //echo "Formulario de añadir categoria";
        return view('categorias.new');

    }

    public function edit($category_id)
    {
        //seleccionar categoria a editar
        $categoria=Categoria::find($category_id);
        //Mostrar la vista de actualizar categoria
        //llevando dentro la categoria
        return view("categorias.edit")->with("categorias", $categoria);
    }

    public function update($category_id)
    {
        //echo $category_id;
        //seleccionar categoria a editar
        $categoria=Categoria::find($category_id);
        //Editar atributos
        $categoria -> name = $_POST["categoria"];
        $categoria->save();
        return redirect("categorias/edit/$category_id")->with("mensaje" , "Categoria Editada");
    }

    //Llegar los datos desde el formulario
    //guardar la cartegoria en BD
    public function store(Request $r){

        //Validacion
        //1.Establecer las reglas de validacipn en cada campo
        $reglas=[
            "categoria"=>["required", "alpha"]
        ];

        $mensajes=[
            "required" => "Campo Obligatorio",
            "alpha"    => "Solo Letras"
        ];

        //crear un objeto validador
        $validador = Validator::make($r->all() , $reglas, $mensajes);
        //validar : meotodo fails 
        //          retorna true(v) si la validacion falla
        //          retorna falso en caso de que los datos sean correctos
        if($validador->fails()){
            //codigo  para cuando falla
            return redirect("categorias/create")->withErrors($validador);
        }else{
            //codugo para cuando la evaluacion es correcta
        }
        //$_POST=arreglo de PHP
        //Almacena  la informacion que viene
        //Desde formularios
        //Crear una nueva categoria
        $categoria=new Categoria();
        //asignar el nombre
        $categoria->name = $r->input("categoria");
        //Guardar nueva categoria
        $categoria->save();
        //Letrero de exito
        //echo "Categoria Guardada";
        //Redireccion con laravel
        return redirect("categorias/create")->with("mensaje","Categoria Guardada");

    }

}
    